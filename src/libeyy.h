#define GLOBAL_EXPORT __attribute__((visibility("default")))

namespace Eyy {
    struct GLOBAL_EXPORT Config {
    public:
        unsigned int val;
    };

    class GLOBAL_EXPORT Lmao {
    public:
        Lmao* Init(const Config&);
    };
};
