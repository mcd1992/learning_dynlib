#include <stdio.h>
#include <dlfcn.h>
#include <string.h>

#define GLOBAL_EXPORT __attribute__((visibility("default")))
#define INITSYMBOL "_ZN3Eyy4Lmao4InitERKNS_6ConfigE"

namespace Eyy {
    struct GLOBAL_EXPORT Config {
    public:
        unsigned int val;
    };

    class GLOBAL_EXPORT Lmao {
    public:
        Lmao* Init(const Config&);
    };

    Lmao* Lmao::Init(const Config &conf) {
        typedef Lmao* (Lmao::*initfunc_type)(const Config&) const; // pointer-to-member type
        fprintf(stderr, "[detour] enter Lmao::Init\n");

        const_cast <Config&>(conf).val = 69;

        static initfunc_type orig_initfunc = 0;
        if (!orig_initfunc) {
            void *initptr = dlsym(RTLD_NEXT, INITSYMBOL);

            // not even reinterpret_cast can convert between a void pointer and a class method pointer,
            memcpy(&orig_initfunc, &initptr, sizeof(&initptr));

            if (!orig_initfunc) {
                fprintf(stderr, "[detour] ERROR: Failed to get original Lmao::Init from symbol!\n");
                return NULL;
            }
        }

        Lmao *retval = (this->*orig_initfunc)(conf);
        return retval;
    }
};

__attribute__((constructor)) void dll_load() { // Alright, this is new to me. And I like it.
    printf("detourdll_load()\n");
}
