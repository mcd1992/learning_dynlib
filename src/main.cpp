#include <cstdio>

#include "libeyy.h"

int main() {
    printf("main()\n");

    Eyy::Config conf;
    conf.val = 7;

    printf("config: 0x%x [%i]\n", conf, conf.val);

    Eyy::Lmao *lmao;
    lmao->Init(conf);

    printf("lmao: 0x%x\n", lmao);
    printf("config: 0x%x [%i]\n", conf, conf.val);

    return 0;
}
