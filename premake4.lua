solution "learning_dylib"
    configurations { "Debug" }
    language "C++"
    location ( _ACTION )

    flags { "Symbols" }
    platforms { "x32" }
    targetdir( "./bin/" )
    configuration( "Debug" )


project "eyy" -- libeyy.so
    kind( "SharedLib" )
    files {"./src/libeyy.cpp" }


project "bin" -- main executable
    kind( "ConsoleApp" )
    files {"./src/main.cpp" }
    links {"eyy"}


project "detourlib" -- ld_preload lib
    kind( "SharedLib" )
    files {"./src/detour.cpp" }
    links { "pthread" } -- needed for dlopen / dlsym
